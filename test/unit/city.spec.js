'use strict'

const { test, trait } = use("Test/Suite")("City");

trait('Test/ApiClient');
trait('DatabaseTransactions');

test('Crear Ciudad Exitosamente', async({ client }) => {
    const attributes = {
        "name": "Chanaral",
        "url": "https://www.accuweather.com/en/cl/chanaral/52502/daily-weather-forecast/52502",
    }
    const response = await client.post("/cities").send(attributes).end();
    response.assertStatus(200)
    response.assertJSONSubset({ "name": "Chanaral", "url": "https://www.accuweather.com/en/cl/chanaral/52502/daily-weather-forecast/52502"});
}).timeout(60000)

test("Listar las ciudades", async({ client }) => {
    const response = await client
        .get(`cities`)
        .send()
        .end();
    response.assertStatus(200)
    response.assertJSONSubset([{
            "name":"Chanaral",
            "url":"https://www.accuweather.com/en/cl/chanaral/52502/daily-weather-forecast/52502"
        }]);
}).timeout(60000)


