'use strict'

const { test, trait } = use("Test/Suite")("Forecast");

trait('Test/ApiClient');
trait('DatabaseTransactions');

test('Crear Forescast Exitosamente', async({ client }) => {
    const attributes = {
        "temphigh": 20,
        "templow": 16,
        "precip": 10,
        "phrase": "Intermitent clouds",
        "date": "2020-05-04",
        "cit_id": 2
    }
    const response = await client.post("/forecast").send(attributes).end();
    response.assertStatus(200)
    response.assertJSONSubset(attributes);
}).timeout(60000)

test("Listar los forecast para una ciudad", async({ client }) => {
    const response = await client
        .get(`cities/1/forecast/01-01-2018`)
        .send()
        .end();
    response.assertStatus(404)
}).timeout(60000)


