'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')

class Forecast extends Model {
  static get table() {
    return Env.get('ESQUEMA') + '.forecast'
  }
}

module.exports = Forecast
