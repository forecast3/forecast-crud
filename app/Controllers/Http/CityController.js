'use strict'

const City = use("App/Models/City");

class CityController {
  async index({ response }) {
    try {
        let cities = await City.query().fetch()
        return response.removeHeader('Set-Cookie', 'Server').status(200).json(cities)
    } catch (err) {
        return response.removeHeader('Set-Cookie', 'Server').status(500).json(err)
    }
  }
  async store({ request, response }) {
      try {
          //Almacenamos la ciudad
          let city = new City()
          city.name = request.input('name')
          city.url = request.input('url')
          await city.save()
          return response.removeHeader('Set-Cookie', 'Server').status(200).json(city)
      } catch (err) {
          return response.removeHeader('Set-Cookie', 'Server').status(500).json(err)
      }
  }
}

module.exports = CityController
