'use strict'

const Forecast = use("App/Models/Forecast");
const moment = require('moment');

class ForecastController {
  async show({ params, response }) {
    try {
        let cit_id = params.id;
        let date = params.date;
        let forecast = await Forecast
                            .query()
                            .where('date', "=", moment(date,"DD-MM-YYYY").format("YYYY-MM-DD"))
                            .andWhere('cit_id',"=",cit_id)
                            .fetch()
        if (forecast.rows.length > 0) {
            return response.removeHeader('Set-Cookie', 'Server').status(200).json(forecast)
        } else {
            return response.removeHeader('Set-Cookie', 'Server').status(404).json("No se encuentra el forecast")
        }
    } catch (err) {
        return response.removeHeader('Set-Cookie', 'Server').status(500).json({ data: err })
    }
  }
  async store({ request, response }) {
    try {
        //Almacenamos el wheather forecast
        let forecast = new Forecast()
        forecast.temphigh = request.input('temphigh')
        forecast.templow = request.input('templow')
        forecast.precip = request.input('precip')
        forecast.phrase = request.input('phrase')
        forecast.date = request.input('date')
        forecast.cit_id = request.input('cit_id')
        await forecast.save()
        return response.removeHeader('Set-Cookie', 'Server').status(200).json(forecast)
    } catch (err) {
        return response.removeHeader('Set-Cookie', 'Server').status(500).json(err)
    }
  }
  async storeMasive({ request, response }) {
    try {
      let datos = request.all();
      let arrForecast = datos.forecast;
      for(let i=0; i< arrForecast.length; i++){
        let forecast = await Forecast
                            .query()
                            .where('date', "=", moment(arrForecast[i]["date"],"YYYY-MM-DD").format("YYYY-MM-DD"))
                            .andWhere('cit_id',"=",arrForecast[i]["cit_id"])
                            .fetch()
        if(forecast.rows.length<=0){
          let forecast = new Forecast()
          forecast.temphigh = arrForecast[i]["temphigh"]
          forecast.templow = arrForecast[i]["templow"]
          forecast.precip = arrForecast[i]["precip"]
          forecast.phrase = arrForecast[i]["phrase"]
          forecast.date = arrForecast[i]["date"]
          forecast.cit_id = arrForecast[i]["cit_id"]
          await forecast.save()
        }
      }
      return response.removeHeader('Set-Cookie', 'Server').status(200).json("Almacenado")
    } catch (err) {
        return response.removeHeader('Set-Cookie', 'Server').status(500).json(err)
    }
  }  
  
}

module.exports = ForecastController
